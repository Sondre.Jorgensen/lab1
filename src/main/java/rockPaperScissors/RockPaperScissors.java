package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.sql.rowset.spi.SyncResolver;



public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() { 
        while (true){
            System.out.printf("Let's play round %s.\n", roundCounter);
            String humanChoice = userChoice();
            String compterChoice = randomChoise();
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, compterChoice);
            
            if (isWinner(humanChoice, compterChoice)){
                System.out.printf(" %s Human wins.\n", choiceString);
                humanScore += 1;
                System.out.printf(" Score: human %s, computer %s.\n ", humanScore, computerScore);
            }
            else if (isWinner(humanChoice, compterChoice)) {
                System.out.printf(" %s Computer wins.\n", compterChoice);
                computerScore += 1;
                System.out.printf(" Score: human %s, computer %s.\n ", humanScore, computerScore);
            }
            else {
                System.out.printf(" %s It's a tie\n", choiceString);
                System.out.printf(" Score: human %s, computer %s.\n ", humanScore, computerScore);
            }
            roundCounter++;
            String contVar = continuePlaying();
            if (contVar.equals("n")) {
                System.out.println("Bye bye :)");
            break;
            }
           
        }    
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
// installere random valg
   public String randomChoise() {
       Random rand = new Random();
       String choices = rpsChoices .get(rand.nextInt(3));
       return choices;
   }

        // innsallere vinner
    public boolean isWinner(String choice1, String choice2) {
        if (choice1 == "paper") {
            return (choice2 == "rock") ;
        } 
        else if (choice1 == "scissors") {
            return (choice2 == "paper");
        }
        else ;
        return (choice2 == "scissors");    
    }
        // installere brukervalg (userchoise)
    public String userChoice() {
        while (true){
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)){
                return humanChoice;
            }
            else {
                System.out.printf("I don't understand %s. Try again\n", humanChoice);
                continue;
            }
        }
    } 
     // installere validate input
    public boolean validateInput(String input, List<String> validInput){
        input = input.toLowerCase();
        return validInput.contains(input);
        }

        // installere continue playing

    public String continuePlaying() {
    String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
        while (true){
        List<String> validYN= Arrays.asList("y", "n");
        if (validateInput(continueAnswer, validYN)) {
            return continueAnswer;
        }
        else {
            System.out.printf("I don't understand %s. Try again\n", continueAnswer);
        }
    }
        
    }

}